package com.c4biz.osgiutils.api.services;


public interface IApplicationModuleService<T extends IApplicationService> {

	public T getApplicationComponentService();
}
