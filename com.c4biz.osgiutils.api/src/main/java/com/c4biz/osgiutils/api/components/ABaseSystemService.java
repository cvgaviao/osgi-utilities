package com.c4biz.osgiutils.api.components;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.log.LogService;

import com.c4biz.osgiutils.api.services.IApplicationService;

public abstract class ABaseSystemService extends ABaseServiceComponent
		implements IApplicationService {

	private EventAdmin eventAdmin;
	
	protected void activate(ComponentContext context,
			Map<String, Object> properties) {
		
		super.activate(context, properties);

	}

	protected void bindEventAdmin(EventAdmin eventAdmin) {
		this.eventAdmin = eventAdmin;
		getLogService().log(LogService.LOG_DEBUG, "Binded EventAdminService.");
	}

	protected EventAdmin getEventAdmin() {
		return eventAdmin;
	}


	protected void postEvent(String eventTopic) {
		Map<String, ?> properties = new HashMap<String, Object>();
		postEvent(eventTopic, properties);

	}

	protected void postEvent(String eventTopic, Map<String, ?> properties) {
		Event event = new Event(eventTopic, properties);
		eventAdmin.postEvent(event);

	}

	protected void sendEvent(String eventTopic) {
		Dictionary<String, ?> properties = new Hashtable<String, Object>();
		sendEvent(eventTopic, properties);

	}

	protected void sendEvent(String eventTopic, Dictionary<String, ?> properties) {
		Event event = new Event(eventTopic, properties);
		eventAdmin.sendEvent(event);

	}

	protected void unbindEventAdmin(EventAdmin eventAdmin) {
		if (this.eventAdmin == eventAdmin) {
			this.eventAdmin = null;
			getLogService().log(LogService.LOG_DEBUG,
					"Unbinded EventAdminService.");
		}
	}
}
