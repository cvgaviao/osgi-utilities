package com.c4biz.osgiutils.api.resources;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.osgi.framework.BundleContext;
import org.osgi.framework.wiring.BundleWiring;

public class LookupResouces {

	public static List<String> scan(BundleContext bundleContextModule,
			String basedir, String includePattern, String excludePattern) {

		List<String> scannedItens = new ArrayList<String>();

		BundleWiring wiring = bundleContextModule.getBundle().adapt(
				BundleWiring.class);

		if (includePattern != null && !includePattern.isEmpty()
				&& wiring != null) {
			Collection<String> foundIncludedFiles = wiring
					.listResources(basedir, includePattern,
							BundleWiring.LISTRESOURCES_RECURSE);
			if (foundIncludedFiles != null && !foundIncludedFiles.isEmpty()) {
				scannedItens.addAll(foundIncludedFiles);
			}
		}

		if (excludePattern != null && !excludePattern.isEmpty()
				&& wiring != null) {
			Collection<String> foundExcludeFiles = wiring
					.listResources(basedir, excludePattern,
							BundleWiring.LISTRESOURCES_RECURSE);
			if (foundExcludeFiles != null && !foundExcludeFiles.isEmpty()) {
				scannedItens.removeAll(foundExcludeFiles);
			}
		}

		return scannedItens;
	}

}
