package com.c4biz.osgiutils.api.events;

public interface AppEventCatalog {

	/**
	 * com/c4biz/feast/events/app/LifecycleEvent/
	 */
	public static String APP_EVENTS = "com/c4biz/feast/events/app/LifecycleEvent/*";
	public static String APP_EVENT_CONFIGURE = "com/c4biz/feast/events/app/LifecycleEvent/CONFIGURE";
	public static String APP_EVENT_STARTED = "com/c4biz/feast/events/app/LifecycleEvent/STARTED";
	public static String APP_EVENT_STOP = "com/c4biz/feast/events/app/LifecycleEvent/STOP";
}
