package com.c4biz.osgiutils.api.components;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.osgi.service.log.LogService;

import com.c4biz.osgiutils.api.events.AppEventCatalog;
import com.c4biz.osgiutils.api.services.IApplicationModuleService;
import com.c4biz.osgiutils.api.services.IApplicationService;

/**
 * 
 * @author cvgaviao
 * 
 * @param <T>
 */
public abstract class ABaseApplicationModuleService<T extends IApplicationService>
		extends ABaseServiceComponent implements IApplicationModuleService<T> {

	private class GenericModuleEventHandler implements EventHandler {

		@Override
		public void handleEvent(Event event) {
			handleApplicationEvent(event);
		}
	}

	private T applicationComponentService;
	private EventAdmin eventAdmin;
	private ServiceRegistration<EventHandler> register;

	public ABaseApplicationModuleService() {

	}

	protected void activate(ComponentContext context,
			Map<String, Object> properties) {
		super.activate(context, properties);

		getLogService().log(LogService.LOG_INFO,
				"Activating " + getHumanReadableComponentName() + " Module...");

		// register generic EventHandler to Deal with Application Events
		// String filter = "(bundle.symbolicName=test.*)";
		Dictionary<String, Object> dict = new Hashtable<String, Object>();
		dict.put(EventConstants.EVENT_TOPIC, AppEventCatalog.APP_EVENTS);
		// dict.put(EventConstants.EVENT_FILTER, filter);

		// Registering the EventHandler
		register = getBundleContext().registerService(EventHandler.class,
				new GenericModuleEventHandler(), dict);
	}

	protected void bindEventAdmin(EventAdmin eventAdmin) {
		this.eventAdmin = eventAdmin;
		getLogService().log(LogService.LOG_DEBUG, "Binded EventAdminService");
	}

	protected void deactivate(ComponentContext context,
			Map<String, Object> properties) {
		super.deactivate(context, properties);

		getLogService().log(LogService.LOG_INFO,
				"Deactivating " + getHumanReadableComponentName() + "...");

		register.unregister();

		// send a post to notify bundles that BPM knowledge is ready to supply
		// sessions...
		// postEvent(AppEventCatalog.BPM_ENGINE_EVENT_STOP);

	}

	protected EventAdmin getEventAdmin() {
		return eventAdmin;
	}


	protected void handleApplicationEvent(Event event) {
		getLogService().log(LogService.LOG_DEBUG,
				"Event from Application was received");
	}

	protected void postEvent(String eventTopic) {
		Map<String, ?> properties = new HashMap<String, Object>();
		postEvent(eventTopic, properties);

	}

	protected void postEvent(String eventTopic, Map<String, ?> properties) {
		Event event = new Event(eventTopic, properties);
		eventAdmin.postEvent(event);

	}

	protected void sendEvent(String eventTopic) {
		Dictionary<String, ?> properties = new Hashtable<String, Object>();
		sendEvent(eventTopic, properties);

	}

	protected void sendEvent(String eventTopic, Dictionary<String, ?> properties) {
		Event event = new Event(eventTopic, properties);
		eventAdmin.sendEvent(event);

	}

	protected void unbindEventAdmin(EventAdmin eventAdmin) {
		if (this.eventAdmin == eventAdmin) {
			this.eventAdmin = null;
			getLogService().log(LogService.LOG_DEBUG,
					"Unbinded EventAdminService");
		}
	}

	public T getApplicationComponentService() {
		return applicationComponentService;
	}

	protected void bindApplicationComponentService(T applicationComponentService) {
		this.applicationComponentService = applicationComponentService;
	}

	protected void unbindApplicationComponentService(
			T applicationComponentService) {
		if (this.applicationComponentService == applicationComponentService)
			this.applicationComponentService = null;
	}

}
