package com.c4biz.osgiutils.api.services.mocks;

import org.osgi.service.log.LogService;

public class ABaseServiceMock {

	private LogService logService;

	public LogService getLogService() {
		return logService;
	}

	public void bindLogService(LogService logService) {
		this.logService = logService;
	}

	public void unbindLogService(LogService logService) {
		if (this.logService == logService)
			this.logService = null;
	}

}
