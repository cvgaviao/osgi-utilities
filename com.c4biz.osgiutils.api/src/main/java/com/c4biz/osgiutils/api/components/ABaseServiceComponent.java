package com.c4biz.osgiutils.api.components;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Map;

import org.osgi.framework.BundleContext;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.log.LogService;

public abstract class ABaseServiceComponent {

	private BundleContext bundleContext;
	private Dictionary<String, Object> cachedConfigurationData;
	private boolean isReady;
	private LogService logService;

	protected void activate(ComponentContext context,
			Map<String, Object> properties) {

		getLogService().log(LogService.LOG_INFO,
				"Activating '" + getHumanReadableComponentName() + "'...");

		Dictionary<String, Object> config = new Hashtable<String, Object>();
		if (properties != null) {
			for (String item : properties.keySet()) {
				config.put(item, properties.get(item));
			}
		}
		cachedConfigurationData = config;

		// save bundleContext reference...
		bundleContext = context.getBundleContext();
		
		isReady = true;

	}

	protected void bindLogService(LogService logService) {
		this.logService = logService;
		getLogService().log(LogService.LOG_DEBUG, "Binded LogService.");
	}

	protected void deactivate(ComponentContext context,
			Map<String, Object> properties) {
		isReady = false;
		cachedConfigurationData = null;
		bundleContext = null;

		getLogService().log(LogService.LOG_INFO,
				"Deactivating '" + getHumanReadableComponentName() + "'...");

	}

	protected BundleContext getBundleContext() {
		return bundleContext;
	}

	protected Dictionary<String, Object> getCachedConfigurationData() {
		return cachedConfigurationData;
	}

	protected abstract String getHumanReadableComponentName();

	protected LogService getLogService() {
		return logService;
	}

	public boolean isReady() {
		return isReady;
	}

	protected void modified(ComponentContext context,
			Map<String, Object> properties) {

		getLogService().log(
				LogService.LOG_INFO,
				getHumanReadableComponentName()
						+ " was modified. Service need to be restarted.");
	}

	protected void unbindLogService(LogService logService) {
		if (this.logService == logService) {
			getLogService().log(LogService.LOG_DEBUG, "Unbinded LogService.");
			this.logService = null;
		}
	}

}
