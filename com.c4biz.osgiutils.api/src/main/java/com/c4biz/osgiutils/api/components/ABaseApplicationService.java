package com.c4biz.osgiutils.api.components;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Map;

import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.osgi.service.log.LogService;

import com.c4biz.osgiutils.api.events.AppEventCatalog;
import com.c4biz.osgiutils.api.services.IApplicationService;

public abstract class ABaseApplicationService extends ABaseSystemService
		implements IApplicationService {

	private class GenericApplicationEventHandler implements EventHandler {

		@Override
		public void handleEvent(Event event) {
			handleApplicationEvent(event);
		}
	}

	private ServiceRegistration<EventHandler> register;

	protected void activate(ComponentContext context,
			Map<String, Object> properties) {
		
		super.activate(context, properties);

		getLogService().log(LogService.LOG_INFO,
				"Activating " + getHumanReadableComponentName() + " Application Service...");

		// register generic EventHandler to Deal with Application Events
		// String filter = "(bundle.symbolicName=test.*)";
		Dictionary<String, Object> dict = new Hashtable<String, Object>();
		dict.put(EventConstants.EVENT_TOPIC, AppEventCatalog.APP_EVENTS);
		// dict.put(EventConstants.EVENT_FILTER, filter);

		// Registering the EventHandler
		register = getBundleContext().registerService(EventHandler.class,
				new GenericApplicationEventHandler(), dict);
	}


	protected void deactivate(ComponentContext context,
			Map<String, Object> properties) {
		
		super.deactivate(context, properties);
		
		//TODO notify all modules before deativate...
		
		getLogService().log(LogService.LOG_INFO,
				"Deactivating " + getHumanReadableComponentName() + " Application Service...");

		register.unregister();

		// send a post to notify bundles that BPM knowledge is ready to supply
		// sessions...
		// postEvent(AppEventCatalog.BPM_ENGINE_EVENT_STOP);

	}

	/**
	 * This method needs to be override by children classes.
	 * 
	 * @param event
	 */
	protected void handleApplicationEvent(Event event) {
		getLogService().log(LogService.LOG_DEBUG,
				"Event from Application was received");
	}
}
