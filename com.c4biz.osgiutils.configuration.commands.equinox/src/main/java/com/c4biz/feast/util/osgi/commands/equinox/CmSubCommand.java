package com.c4biz.feast.util.osgi.commands.equinox;

import java.io.PrintStream;
import java.util.List;

import org.osgi.framework.BundleContext;

public interface CmSubCommand {

	public void execute(BundleContext context, String cmd, List<String> args,
			String commandLine, PrintStream out, PrintStream err);
}
