package com.c4biz.feast.util.osgi.commands.equinox;

import java.io.IOException;
import java.util.Enumeration;
import java.util.List;

import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.cm.Configuration;

public class ListCommand extends AbstractCmSubCommand {

	@Override
	protected boolean needsPid() {
		return false;
	}

	@Override
	protected void doCommand(BundleContext context, String cmd,
			List<String> args, String commandLine) throws IOException {

		try {
			Configuration[] configurations;
			configurations = configurationAdmin.listConfigurations(null);
			out("Configuration list:");
			if (configurations == null || configurations.length == 0) {
				out("   (none)");
				return;
			}
			int maxPidLength = 0;
			for (Configuration c : configurations) {
				if (c.getPid().length() > maxPidLength) {
					maxPidLength = c.getPid().length();
				}
			}
			int tabPosition = maxPidLength + 4;
			for (Configuration c : configurations) {
				String pid = c.getPid();
				String bundleLocation = c.getBundleLocation();
				String tab = "";
				for (int i = tabPosition; i >= pid.length(); i--)
					tab += " ";
				out("* " + pid
						+ (bundleLocation != null ? tab + bundleLocation : ""));
				Enumeration<?> properties = c.getProperties().keys();
				while (properties.hasMoreElements()) {
					String key = (String) properties.nextElement();
					String value = (String) c.getProperties().get(key);
					if (!key.equals("service.pid"))
						out(tab + key + "=" + value);
				}
			}
		} catch (InvalidSyntaxException e) {
			// impossible
		}
	}
}
