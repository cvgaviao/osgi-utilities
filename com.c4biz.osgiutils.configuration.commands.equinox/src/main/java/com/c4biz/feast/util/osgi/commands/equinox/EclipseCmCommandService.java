package com.c4biz.feast.util.osgi.commands.equinox;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.osgi.framework.console.CommandInterpreter;
import org.eclipse.osgi.framework.console.CommandProvider;
import org.osgi.framework.BundleContext;

import com.c4biz.osgiutils.api.components.ABaseSystemService;

public class EclipseCmCommandService extends ABaseSystemService implements
		CommandProvider {

	protected BundleContext bundleContext;

	public EclipseCmCommandService() {
	}

	public EclipseCmCommandService(BundleContext context) {
		this.bundleContext = context;
	}

	public String getHelp() {
		return "\tcm help|list|get... - Access OSGi Configuration Admin service";
	}

	public void _cm(CommandInterpreter ci) {
		List<String> args = new ArrayList<String>();
		String arg = null;
		while ((arg = ci.nextArgument()) != null) {
			args.add(arg);
		}

		new CmCommandProcessor(bundleContext).execute(args, null, System.out,
				System.err);
	}

	@Override
	protected String getHumanReadableComponentName() {
		
		return "OSGi Utilities Configuration Manager";
	}
}
