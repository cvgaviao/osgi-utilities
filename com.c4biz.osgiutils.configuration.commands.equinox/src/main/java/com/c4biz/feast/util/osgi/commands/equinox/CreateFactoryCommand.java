package com.c4biz.feast.util.osgi.commands.equinox;

import java.io.IOException;
import java.util.Hashtable;
import java.util.List;

import org.osgi.framework.BundleContext;
import org.osgi.service.cm.Configuration;

public class CreateFactoryCommand extends AbstractCmSubCommand {

	@Override
	protected void doCommand(final BundleContext context, final String cmd,
			final List<String> args, final String commandLine)
			throws IOException {
		final Configuration configuration;
		if (args.size() > 0) {
			configuration = configurationAdmin.createFactoryConfiguration(pid,
					(String) args.get(0));
		} else {
			// Create with null location, will be bound to the bundle that uses
			// it.
			configuration = configurationAdmin.createFactoryConfiguration(pid, null);
		}
		// Ensure update is called, when properties are null; otherwise
		// configuration will not
		// be returned when listConfigurations is called (see specification
		// 104.15.3.7)
		if (configuration.getProperties() == null) {
			configuration.update(new Hashtable<String, String>());
		}

		out("PID: " + configuration.getPid());
	}

}
