package com.c4biz.feast.util.osgi.commands.equinox;

import java.io.IOException;
import java.util.List;

import org.osgi.framework.BundleContext;
import org.osgi.service.cm.Configuration;

public class DeleteCommand extends AbstractCmSubCommand {

	@Override
	protected void doCommand(BundleContext context, String cmd,
			List<String> args, String commandLine) throws IOException {

		Configuration configuration = findConfiguration(pid);
		if (configuration != null)
			configuration.delete();
		else
			out.println("no configuration for pid '" + pid + "'");
	}
}
