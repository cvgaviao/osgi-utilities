package com.c4biz.osgiutils.logging.reader;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.log.LogReaderService;

/**
 * Currently only tracks LogReaderService
 */
public class LogReaderComponent {

	private Map<LogReaderService, OsgiLogListener> logReaderMap = Collections
			.synchronizedMap(new HashMap<LogReaderService, OsgiLogListener>());
	
	public void activate(ComponentContext context) {}

	public void deactivate(ComponentContext context) {}
	
	public void addLogReaderService(LogReaderService aLogReaderService) {
		OsgiLogListener listener = new OsgiLogListener();
		logReaderMap.put(aLogReaderService, listener);
		aLogReaderService.addLogListener(listener);
	}
	
	public void removeLogReaderService(LogReaderService aLogReaderService) {
		OsgiLogListener listener = logReaderMap.get(aLogReaderService);
		if (listener != null) {
			aLogReaderService.removeLogListener(listener);
		}
		logReaderMap.remove(aLogReaderService);
	}
	
}